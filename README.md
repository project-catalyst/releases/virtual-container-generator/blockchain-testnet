# Ethereum PoA network
This repository is used to bootstrap a real Ethereum ***Proof of Authority*** network using Docker.

The following containers are deployed:
*   A geth bootnode
*   Two (2) PoA geth miners (sealers)
*   Geth node with remote RPC open & blockchain visualization

### Required software
1. Docker [engine](https://docs.docker.com/install/linux/docker-ce/ubuntu/). 
2. Docker [compose](https://docs.docker.com/compose/install/).

## Project structure

* **docker-compose.yml**: the docker-compose configuration
* **.env**: global env variables used during ``docker-compose up``.
* **bootnode**: The boot node that all nodes connect to find neighbouring nodes
    * Dockerfile: *the dockerfile to bootstrap the bootnode*
    * boot.key: *the hexkey used to generate the enode of the bootnode*
* **miner**: The 'sealer' blueprint that generates the blocks
    * accounts: Contains the keystore and account addresses of every deployed miner node
    * Dockerfile: *the dockerfile to generate mining nodes*
    * genesis.json: *the genesis block as created with ``puppeth``*
* **explorer**: The blueprint for a geth node + an explorer to visualize the blockchain
    * accounts: Contains the keystore and account address of the explorer node
    * Dockerfile: *the dockerfile to bootstrap the explorer node + server*
    * genesis.json: *the genesis block as created with ``puppeth``*
    * geth-supervisor.conf: *the .conf file to run the geth node as supervisor daemon*
    * run.sh: *the configuration script run at the end of the dockerfile to setup the explorer server*
    
## Components analysis

### Gotchas
1. The ``genesis.json`` is generated with the ``puppeth`` command (recommended)
2. The bootnode/boot.key is used to generate the bootnode enode and it is passed as env variable in the other nodes inside the docker-compose
3. Because only the sealers can generate new blocks, the two sealers and the explorer node is prepopulated with large amounts of ether.


### bootnode
The enode can be generated from the hexkey stored in ``boot.key`` with the following command:
```bash
bootnode -nodekeyhex boot.key -writeaddress
```

The 30301 upd port is mapped to the 30301 external port so that remote geth nodes can communicate with the bootnode.

The output of the previous command is copied as env variable to other nodes in docker-compose.yml

### miner
The miner accounts need to be known before hand because they need to be prepopulated with ether in the ``genesis.json``. This is because there is not *mining* in a PoA network.

The accounts are generated with the following command, and the address & password is stored in separate files:
```bash
geth account new --datadir ${miner_folder}
```

Every mining node has a docker ``volume`` to store the chain history so that the chain state is persisted on restarts.

### explorer
The explorer geth node is setup with remote rcp open, and should be used to give ether to other nodes that need to execute transactions in the blockchain. 

The rpc port is 8545 and is mapped to the 8545 external port.

The explorer node server is listening to port 8000 and is mapped to the 8000 external port by default.

One gotcha is that the ``EXPLORER_EXTERNAL_IP`` env variable must be set in the docker-compose to match the IP of the deployed VM / system.
This is because the server implementation is done in frontend javascript, and the frontend needs to connect to a geth node. The external ip can be specified during startup as:
```bash
echo "EXPLORER_EXTERNAL_IP=192.168.1.1" > .env
```

## Starting the blockchain
Run the following to start all the nodes:
```bash
docker-compose up
```