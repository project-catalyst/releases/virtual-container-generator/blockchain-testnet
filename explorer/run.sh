#!/bin/bash

# Run geth client as supervisor deamon
address=$(</opt/ethereum/accounts.txt)
sed -i "s/bootnodeId/$bootnodeId/g" /etc/supervisor/conf.d/geth-supervisor.conf
sed -i "s/bootnodeIp/$bootnodeIp/g" /etc/supervisor/conf.d/geth-supervisor.conf
sed -i "s/address/$address/g" /etc/supervisor/conf.d/geth-supervisor.conf
service supervisor start

# If the remote git repo exists, it is not fetched again
if [ -d "/opt/ethereum/explorer" ]; then
  echo "Folder exists. Skipping git clone."
  cd /opt/ethereum/explorer
else
  echo "Fetching git repo..."
  cd /opt/ethereum
  git clone https://github.com/carsenk/explorer
  cd explorer
  sed -i "s/-a localhost -p 8000/-a 0.0.0.0 -p 8000/g" package.json
  sed -i "s/var GETH_HOSTNAME	= \"localhost\"/var GETH_HOSTNAME	= \"$externalIp\"/g" app/app.js
fi

npm install
bower install
npm start
